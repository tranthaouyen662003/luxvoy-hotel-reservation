import { RegisterDto } from 'src/user/dto/register.dto';

export class RegisterCommand {
  constructor(public readonly dto: RegisterDto) {}
}
