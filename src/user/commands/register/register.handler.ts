import { BadRequestException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { OkResponse } from '../../responses/ok.response';
import { User } from 'src/user/schema/user.schema';
import { RegisterCommand } from './register.command';

@CommandHandler(RegisterCommand)
export class RegisterHandler implements ICommandHandler<RegisterCommand> {
  constructor(
    @InjectModel('users')
    private readonly userModel: Model<User>,
  ) {}

  async execute(command: RegisterCommand): Promise<any> {
    const { fullName, phoneNumber, address, password } = command.dto;
    const { street, ward, district } = address;

    const userExists = await this.userModel.exists({ phoneNumber }).exec();
    if (userExists) {
      throw new BadRequestException('PhoneNumber already exists!');
    }

    const newUser = new this.userModel({
      username: phoneNumber,
      password,
      fullName,
      phoneNumber,
      address: {
        street,
        ward,
        district,
      },
    });

    await newUser.save();
    return new OkResponse({ newUser });
  }
}
