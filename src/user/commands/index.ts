import { LoginHandler } from './login/login.handler';
import { RegisterHandler } from './register/register.handler';
export const commandHandlers = [RegisterHandler, LoginHandler];
