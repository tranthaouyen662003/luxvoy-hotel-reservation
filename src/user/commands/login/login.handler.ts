import { BadRequestException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { OkResponse } from '../../responses/ok.response';
import { User } from 'src/user/schema/user.schema';
import { LoginCommand } from './login.command';

@CommandHandler(LoginCommand)
export class LoginHandler implements ICommandHandler<LoginCommand> {
  constructor(
    @InjectModel('users')
    private readonly userModel: Model<User>,
  ) {}

  async execute(command: LoginCommand): Promise<any> {
    const phoneNumber = command.dto.username;
    const userToAttempt = await this.userModel.findOne({
      phoneNumber,
    });

    if (userToAttempt == null) {
      throw new BadRequestException('Invalid phone number or password');
    }

    const isMatch = userToAttempt.password === command.dto.password;
    if (!isMatch)
      throw new BadRequestException('Invalid phone number or password');

    return new OkResponse({ userToAttempt });
  }
}
