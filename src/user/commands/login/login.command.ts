import { LoginDto } from 'src/user/dto/login.dto';

export class LoginCommand {
  constructor(public readonly dto: LoginDto) {}
}
