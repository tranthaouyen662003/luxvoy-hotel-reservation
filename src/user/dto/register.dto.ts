import { ApiProperty } from '@nestjs/swagger';

export class RegisterAddress {
  @ApiProperty()
  street: string;

  @ApiProperty()
  ward: string;

  @ApiProperty()
  district: string;
}

export class RegisterDto {
  @ApiProperty()
  phoneNumber: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  fullName: string;

  @ApiProperty()
  address: RegisterAddress;
}
