import { AutoMap } from '@automapper/classes';

export class UserRo {
  @AutoMap()
  id: string;
  @AutoMap()
  email: string;
  @AutoMap()
  username: string;
  @AutoMap()
  role: number;
}
