import { AutoMap } from '@automapper/classes';

export class AuthRo {
  @AutoMap()
  id: string;
  @AutoMap()
  email: string;
  @AutoMap()
  username: string;
  @AutoMap()
  role: number;
}
