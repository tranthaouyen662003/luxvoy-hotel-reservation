import { Body, Controller, Get, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { InjectModel } from '@nestjs/mongoose';
import { ApiTags } from '@nestjs/swagger';
import { Model } from 'mongoose';
import { LoginCommand } from './commands/login/login.command';
import { RegisterCommand } from './commands/register/register.command';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { AuthRo } from './response-objects/auth.ro';
import { BaseResponse } from './responses/base.response';
import { User } from './schema/user.schema';

@Controller('user')
@ApiTags('auth')
export class UserController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    @InjectModel('users')
    private readonly userModel: Model<User>,
  ) {}
  @Post('register')
  async register(@Body() dto: RegisterDto): Promise<AuthRo> {
    return this.commandBus.execute(new RegisterCommand(dto));
  }

  @Post('login')
  async login(@Body() dto: LoginDto): Promise<BaseResponse<AuthRo>> {
    return this.commandBus.execute(new LoginCommand(dto));
  }

  @Get('user')
  async getUsers(query: object): Promise<User[]> {
    return this.userModel.find(query);
  }
}
